﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadSpaceBehaviour : MonoBehaviour
{
    public GameObject bullet;

    void OnCollisionEnter(Collision col)
    {

        if (col.gameObject.tag == "Bullet")
        {
            // object which collided
            Destroy(col.gameObject);

            Instantiate(bullet, new Vector3(0f, 2f, 2f), Quaternion.identity);
        }
        
    }

}
