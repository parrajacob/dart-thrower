﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement;

public class AssetsMgr : MonoBehaviour
{

    public AssetReference remoteBullet;
    
    // Start is called before the first frame update
    void Start()
    {
        remoteBullet.InstantiateAsync(new Vector3(0f, 2f, 2f), Quaternion.identity);
    }

}
