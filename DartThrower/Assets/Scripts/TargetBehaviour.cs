﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetBehaviour : MonoBehaviour
{

    public GameObject bullet;

    public GameObject target;

    public AudioSource audio;


    void OnCollisionEnter(Collision col) 
    {

        if (col.gameObject.tag == "Bullet")
        {

            audio.Play();
            
            // object which collided
            Destroy(col.gameObject);
            // object with which it collided
            Destroy(gameObject);

            Instantiate(bullet, new Vector3(0f, 2f, 2f), Quaternion.identity);

            float x = Random.Range(-6.0f, 6.0f);
            float y = Random.Range(0.0f, 5.0f);

            Instantiate(target, new Vector3(x , y, 10f), Quaternion.Euler(new Vector3(90, 0, 0)));

            // calling AddScore method
            ScoreBehaviour.AddScore();

        }
    }
}
