﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreBehaviour : MonoBehaviour
{
    private TextMeshProUGUI Text;
    private static int score;
    
    void Start()
    {
        Text = FindObjectOfType<TextMeshProUGUI>();
        
        // set score value to be zero
        score = 0;
    }
    
    void Update()
    {
        // update text of Text element
        Text.text = "Score is " + score;
    }
    
    // adding a new method to our class
    public static void AddScore()
    {
        // add 500 points to score
        score += 500;
    }

}
